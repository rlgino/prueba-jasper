create table persona (
        id varchar(50) primary key,
        nombre varchar(50)
);
create table empleado (
        persona_id varchar(50),
        cargo varchar(50)
);
ALTER TABLE empleado
ADD FOREIGN KEY (persona_id) REFERENCES persona(id);
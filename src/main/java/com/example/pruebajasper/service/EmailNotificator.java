package com.example.pruebajasper.service;

import javax.mail.MessagingException;

public interface EmailNotificator {
    public void execute(String subject, String mailTo) throws MessagingException;
}

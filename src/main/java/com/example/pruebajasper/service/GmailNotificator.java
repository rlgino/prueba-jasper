package com.example.pruebajasper.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

@Service
public class GmailNotificator implements EmailNotificator {

    @Autowired
    private JavaMailSender javaMailSender;

    @Override
    public void execute(String subject, String mailTo) throws MessagingException {
        MimeMessage msg = javaMailSender.createMimeMessage();

        // true = multipart message
        MimeMessageHelper helper = new MimeMessageHelper(msg, true);

        helper.setFrom("facturacion@tornillito.com");
        helper.setTo(mailTo);
        helper.setSubject(subject);

        // default = text/plain
        //helper.setText("Check attachment for image!");

        // true = text/html
        String bodyMsg = "<h1>Te enviamos tu factura!</h1>";
        helper.setText(bodyMsg, true);

        // hard coded a file path
        //FileSystemResource file = new FileSystemResource(new File("path/android.png"));

/*
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		JasperExportManager.exportReportToPdfStream(reportePrint, baos);
		DataSource aAttachment = new ByteArrayDataSource(baos.toByteArray(), "application/pdf");

		MimeMessage message = mailSender.createMimeMessage();

		helper.addAttachment("report.pdf",aAttachment);
*/

        javaMailSender.send(msg);
    }
}

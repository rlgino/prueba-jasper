package com.example.pruebajasper;

import com.example.pruebajasper.service.EmailNotificator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;

import javax.activation.DataSource;
import javax.mail.MessagingException;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.util.ByteArrayDataSource;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

@SpringBootApplication
public class PruebaJasperApplication {
	@Autowired
	EmailNotificator notificator;

	public static void main(String[] args) {
		SpringApplication.run(PruebaJasperApplication.class, args);
	}

}

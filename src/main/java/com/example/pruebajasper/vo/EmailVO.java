package com.example.pruebajasper.vo;

import lombok.Getter;

@Getter
public class EmailVO {
    private String value;

    public EmailVO(String value) {
        this.value = value;
    }
}

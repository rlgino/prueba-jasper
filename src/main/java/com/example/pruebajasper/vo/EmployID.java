package com.example.pruebajasper.vo;

import lombok.Getter;

@Getter
public class EmployID {
    public String value;

    public EmployID(String value) {
        this.value = value;
    }
}

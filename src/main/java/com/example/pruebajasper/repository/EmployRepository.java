package com.example.pruebajasper.repository;

import com.example.pruebajasper.dto.Empleado;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Repository
@Transactional
public interface EmployRepository extends PersonBaseRepository<Empleado> {
    @Override
    Optional<Empleado> findById(String s);
}

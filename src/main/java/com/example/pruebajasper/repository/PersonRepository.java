package com.example.pruebajasper.repository;

import com.example.pruebajasper.dto.Persona;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public interface PersonRepository extends PersonBaseRepository<Persona> {
}

package com.example.pruebajasper.repository;

import com.example.pruebajasper.dto.Persona;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface PersonBaseRepository<T extends Persona> extends CrudRepository<T, String> {
}

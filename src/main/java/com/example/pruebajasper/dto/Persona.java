package com.example.pruebajasper.dto;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public class Persona {
    @Id
    private String id;
    @Column
    private String nombre;
}

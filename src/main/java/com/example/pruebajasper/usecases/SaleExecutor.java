package com.example.pruebajasper.usecases;

import com.example.pruebajasper.dto.Empleado;
import com.example.pruebajasper.repository.EmployRepository;
import com.example.pruebajasper.service.EmailNotificator;
import com.example.pruebajasper.vo.EmailVO;
import com.example.pruebajasper.vo.EmployID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import java.util.Optional;

@Service
public class SaleExecutor {
    @Autowired
    EmailNotificator notificator;
    @Autowired
    EmployRepository employRepo;

    public void execute(EmailVO email, EmployID employID) throws MessagingException {
        notificator.execute("Envio de mail para pruebas", email.getValue());
        final Optional<Empleado> result = employRepo.findById(employID.value);
        result.ifPresent(employ -> {;
            System.out.println("El cargo del empleado es: " + employ.getCargo());
        });
    }
}

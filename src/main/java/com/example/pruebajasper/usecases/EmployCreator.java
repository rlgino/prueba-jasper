package com.example.pruebajasper.usecases;

import com.example.pruebajasper.dto.Empleado;
import com.example.pruebajasper.dto.Persona;
import com.example.pruebajasper.repository.EmployRepository;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EmployCreator {
    @Autowired
    EmployRepository employRepo;

    public void execute(String personID, String cargo) throws NotFoundException {
        final Persona p = new Persona();
        p.setId(personID);

        final Empleado empleado = new Empleado();
        empleado.setId(p.getId());
        empleado.setNombre(p.getNombre());
        empleado.setCargo(cargo);
        employRepo.save(empleado);
    }
}

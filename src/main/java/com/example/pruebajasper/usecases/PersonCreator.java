package com.example.pruebajasper.usecases;

import com.example.pruebajasper.dto.Persona;
import com.example.pruebajasper.repository.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class PersonCreator {
    @Autowired
    PersonRepository personRepository;

    public void execute(String personID, String name) throws RuntimeException {
        final Optional<Persona> person = personRepository.findById(personID);
        if (person.isPresent())
            throw new RuntimeException();

        final Persona persona = new Persona();
        persona.setId(personID);
        persona.setNombre(name);
        personRepository.save(persona);
    }
}

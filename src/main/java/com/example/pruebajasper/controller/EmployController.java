package com.example.pruebajasper.controller;

import com.example.pruebajasper.controller.requests.NewEmployBody;
import com.example.pruebajasper.usecases.EmployCreator;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class EmployController {

    @Autowired
    EmployCreator creator;

    @PostMapping("/employ")
    public ResponseEntity<String> createEmploy(@RequestBody() NewEmployBody body){
        try {
            creator.execute(body.getId(), body.getCargo());
            return new ResponseEntity<>(HttpStatus.CREATED);
        }catch (NotFoundException e) {
            return new ResponseEntity<>("No se encontro persona", HttpStatus.NOT_FOUND);
        }
    }
}

package com.example.pruebajasper.controller.requests;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class NewPersonBody {
    private String id;
    private String nombre;
}

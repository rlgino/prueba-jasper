package com.example.pruebajasper.controller.requests;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FinishSaleBody {
    private String email;
    @JsonProperty("employ")
    private String employID;
}

package com.example.pruebajasper.controller.requests;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class NewEmployBody {
    private String id;
    private String cargo;
}

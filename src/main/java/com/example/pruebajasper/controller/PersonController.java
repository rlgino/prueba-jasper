package com.example.pruebajasper.controller;

import com.example.pruebajasper.controller.requests.NewPersonBody;
import com.example.pruebajasper.usecases.PersonCreator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PersonController {

    @Autowired
    PersonCreator creator;

    @PostMapping("/person")
    public ResponseEntity<String> createPerson(@RequestBody() NewPersonBody body){
        try{
            creator.execute(body.getId(), body.getNombre());
            return new ResponseEntity<>(HttpStatus.CREATED);
        }catch (RuntimeException e) {
            return new ResponseEntity<>("Persona duplicada", HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}

package com.example.pruebajasper.controller;

import com.example.pruebajasper.controller.requests.FinishSaleBody;
import com.example.pruebajasper.usecases.SaleExecutor;
import com.example.pruebajasper.vo.EmailVO;
import com.example.pruebajasper.vo.EmployID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.mail.MessagingException;

@RestController
public class FinishSaleController {
    @Autowired
    SaleExecutor executor;

    @PostMapping("/sale/finishing")
    public void finishSale(@RequestBody() FinishSaleBody body) {
        System.out.println("Procesando...");
        final EmailVO emailVO = new EmailVO(body.getEmail());
        final EmployID employID = new EmployID(body.getEmployID());
        try {
            executor.execute(emailVO,employID);
        } catch (MessagingException e) {
            System.err.println(e.getMessage());
        }
    }
}
